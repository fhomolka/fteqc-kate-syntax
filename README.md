# QuakeC Syntax Highlighting for Kate
FTE flavour

Based on [this gist](https://gist.github.com/DefaultUser/998f030ab41a9e8edf4a9f8e703c6350)

## How to use?
In any way you want, acquire the `fteqc.xml` file from this repo.

### Windows
Your syntax definitions are most likely in:
`%APPDATA%/org.kde.syntax-highlighting/syntax`

Put `fteqc.xml` in that directory and start Kate.

### Linux
Your syntax definitions are most likely in:
`$HOME/.local/share/org.kde.syntax-highlighting/syntax/`
If the directory doesn't exist, make it, Kate see it.

Put `fteqc.xml` in that directory and start Kate.

### Snap, Flatpak and others
Please check [this page](https://docs.kde.org/trunk5/en/kate/katepart/highlight.html)

## Special Thanks to:
* [DefaultUser](https://github.com/DefaultUser) for writing the gist
* [Spike](https://fte.triptohell.info/moodles/fteqcc/README.html) and [Eukara](https://fteqcc.org/dl/fteqcc_manual.txt) for writing and maintaining FTE apps and docs

